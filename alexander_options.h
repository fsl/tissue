/*  meanoptions.h

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  Part of FSL - FMRIB's Software Library
    http://www.fmrib.ox.ac.uk/fsl
    fsl@fmrib.ox.ac.uk
    
    Developed at FMRIB (Oxford Centre for Functional Magnetic Resonance
    Imaging of the Brain), Department of Clinical Neurology, Oxford
    University, Oxford, UK
    
*/

#if !defined(Alexander_Options_h)
#define Alexander_Options_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"
#include "mybintoptions.h"

using namespace Utilities;

namespace Bint {

class Alexander_Options : public BintOptions {
 public:
  static Alexander_Options& getInstance();
  ~Alexander_Options() { delete gopt; }
  
  Option<string> bvecsfile;  
  Option<string> bvalsfile;
  Option<string> besselfile;
  
  
 private:
  Alexander_Options();  
  const Alexander_Options& operator=(Alexander_Options&);
  Alexander_Options(Alexander_Options&);
      
  static Alexander_Options* gopt;
  
};

 inline Alexander_Options& Alexander_Options::getInstance(){
   if(gopt == NULL)
     gopt = new Alexander_Options();
   
   return *gopt;
 }

 inline Alexander_Options::Alexander_Options() :
   BintOptions("alexander_model", "alexander_model --verbose\n"),   
   bvecsfile(string("-r,--bvecs"),"bvecs", 
	     string("gradient directions"), 
	     true, requires_argument),
   bvalsfile(string("-b,--bvals"),"bvals", 
	     string("b values"), 
	     true, requires_argument),
   besselfile(string("--bessel"),"", 
	     string("bessel roots"), 
	     true, requires_argument)

   {
     try {
       options.add(bvecsfile);
       options.add(bvalsfile);
		options.add(besselfile);
     }
     catch(X_OptionError& e) {
       options.usage();
       cerr << endl << e.what() << endl;
     } 
     catch(std::exception &e) {
       cerr << e.what() << endl;
     }    
     
   }
}

#endif





