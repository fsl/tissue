/*  bintoptions.h

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  Part of FSL - FMRIB's Software Library
    http://www.fmrib.ox.ac.uk/fsl
    fsl@fmrib.ox.ac.uk
    
    Developed at FMRIB (Oxford Centre for Functional Magnetic Resonance
    Imaging of the Brain), Department of Clinical Neurology, Oxford
    University, Oxford, UK
    
*/

#if !defined(BintOptions_h)
#define BintOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

using namespace Utilities;

namespace Bint {

class BintOptions {

 public:

  virtual ~BintOptions(){};

  Option<bool> verbose;
  Option<int> debuglevel;
  Option<bool> timingon;
  Option<bool> help;
  Option<string> datafile;
  Option<string> maskfile;
  Option<string> logdir;
  Option<bool> forcedir;
  Option<string> inference;
  Option<int> njumps;
  Option<int> burnin;
  Option<int> sampleevery;
  Option<int> updateproposalevery;
  Option<float> acceptancerate;
  Option<int> seed;
  Option<float> prec;
  Option<bool> analmargprec;
  Option<string> noisemodel;

  void parse_command_line(int argc, char** argv, Log& logger);
  
 protected:

  BintOptions(const string& str1, const string& str2);  
  OptionParser options; 

 private:

  BintOptions();
  const BintOptions& operator=(BintOptions&);
  BintOptions(BintOptions&);
  
};

 inline BintOptions::BintOptions(const string& str1, const string& str2) :
   verbose(string("-v,-V,--verbose"), false, 
	   string("switch on diagnostic messages"), 
	   false, no_argument),
   debuglevel(string("--debug,--debuglevel"), 0,
		       string("set debug level"), 
		       false, requires_argument),
   timingon(string("--to,--timingon"), false, 
		       string("turn timing on"), 
		       false, no_argument),
   help(string("-h,--help"), false,
		    string("display this message"),
		    false, no_argument),
   datafile(string("--data,--datafile"), string("data"),
			  string("data regressor data file"),
		     true, requires_argument),
   maskfile(string("--mask,--maskfile"), string(""),
			  string("mask file"),
		     true, requires_argument),
   logdir(string("--ld,--logdir"), string("logdir"),
			  string("log directory (default is logdir)"),
		     false, requires_argument), 
   forcedir(string("--forcedir"), false,
		    string("Use the actual directory name given - i.e. don't add + to make a new directory"),
		    false, no_argument),
   inference(string("--inf,--inference"), string("mcmc"),
			  string("inference technique: mcmc/laplace (default is mcmc)"),
		     false, requires_argument),  
   njumps(string("--nj,--njumps"), 5000,
			  string("Num of jumps to be made by MCMC (default is 5000)"),
		     false, requires_argument),
   burnin(string("--bi,--burnin"), 500,
			  string("Num of jumps at start of MCMC to be discarded (default is 500)"),
		     false, requires_argument),
   sampleevery(string("--se,--sampleevery"), 1,
			  string("Num of jumps for each sample (MCMC) (default is 1)"),
		     false, requires_argument),
   updateproposalevery(string("--upe,--updateproposalevery"), 40,
		       string("Num of jumps for each update to the proposal density std (MCMC) (default is 40)"),
		     false, requires_argument),
   acceptancerate(string("--arate,--acceptancerate"), 0.6,
			  string("Acceptance rate to aim for (MCMC) (default is 0.6)"),
		     false, requires_argument),
   seed(string("--seed"), 10, 
      string("seed for pseudo random number generator"), 
      false, requires_argument),
   prec(string("--prec"), -1, 
      string("value to fix error precision to (default is -1, which means error precision is not fixed)"), 
	false, requires_argument),
   analmargprec(string("--noamp"), true, 
      string("turn off Analytical Marginalisation of error Precision"), 
	false, no_argument),
   noisemodel(string("--noise"), string("gaussian"), 
		string("model of noise: gaussian/rician"), 
		false, requires_argument),
   options(str1,str2)
   {
     try {
       options.add(verbose);
       options.add(debuglevel);
       options.add(timingon);
       options.add(help);
       options.add(datafile);
       options.add(maskfile);
	   options.add(logdir);
       options.add(forcedir);
       options.add(inference);
       options.add(njumps);
       options.add(burnin);
       options.add(sampleevery);
       options.add(updateproposalevery);
       options.add(acceptancerate);
       options.add(seed);
       options.add(prec);
       options.add(analmargprec);
	   options.add(noisemodel);
	}
     catch(X_OptionError& e) {
       options.usage();
       cerr << endl << e.what() << endl;
     } 
     catch(std::exception &e) {
       cerr << e.what() << endl;
     }    
     
   }
}

#endif



