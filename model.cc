/*  model.cc

Mark Woolrich, FMRIB Image Analysis Group

Copyright (C) 2002 University of Oxford  */

/*  Part of FSL - FMRIB's Software Library
    http://www.fmrib.ox.ac.uk/fsl
    fsl@fmrib.ox.ac.uk
    
    Developed at FMRIB (Oxford Centre for Functional Magnetic Resonance
    Imaging of the Brain), Department of Clinical Neurology, Oxford
    University, Oxford, UK
    
    
*/

#include "model.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"

using namespace Utilities;
using namespace MISCMATHS;

namespace Bint {
 
}
