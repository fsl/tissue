/* Alexander's paper model

Rosario Sance, 26/11/07
Last modified: 12/12/07

*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#define WANT_STREAM
#define WANT_MATH
#include <string>
#include <math.h>
#include "utils/log.h"
#include "alexander_options.h"
#include "mybintoptions.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"
#include "stdlib.h"
#include "model.h"
#include "mylsmcmcmanager.h"
#include "mylslaplacemanager.h"
#include "newimage/newimageall.h"

#define delta 0.018
//in sec.

//#define DELTA 0.01
//#define gamma 267512898

#define Gmag 40*1e-6  
//in T/mm
#define gamma 2*M_PI*4258*1e4
//Hz/T

using namespace Bint;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

Matrix besselroots;
Matrix Besselroots2;
const float maxfloat=1e10;
const float minfloat=1e-10;
const float maxlogfloat=23;
const float minlogfloat=-23;

inline float min(float a,float b){
  return a<b ? a:b;}
inline float max(float a,float b){
  return a>b ? a:b;}

inline Matrix Anis()
{ 
  Matrix A(3,3);
  A << 1 << 0 << 0
    << 0 << 0 << 0
    << 0 << 0 << 0;
  return A;
}
inline Matrix Is()
{ 
  Matrix I(3,3);
  I << 1 << 0 << 0
    << 0 << 1 << 0
    << 0 << 0 << 1;
  return I;
}
inline ColumnVector Cross(const ColumnVector& A,const ColumnVector& B)
{
  ColumnVector res(3);
  res << A(2)*B(3)-A(3)*B(2)
      << A(3)*B(1)-A(1)*B(3)
      << A(1)*B(2)-B(1)*A(2);
  return res;
}
inline Matrix Cross(const Matrix& A,const Matrix& B)
{
  Matrix res(3,1);
  res << A(2,1)*B(3,1)-A(3,1)*B(2,1)
      << A(3,1)*B(1,1)-A(1,1)*B(3,1)
      << A(1,1)*B(2,1)-B(1,1)*A(2,1);
  return res;
}

Matrix form_Amat(const Matrix& r,const Matrix& b)	
{
  Matrix A(r.Ncols(),7);
  Matrix tmpvec(3,1), tmpmat;
  
  for( int i = 1; i <= r.Ncols(); i++){
    tmpvec << r(1,i) << r(2,i) << r(3,i);
    tmpmat = tmpvec*tmpvec.t()*b(1,i);
    A(i,1) = tmpmat(1,1);
    A(i,2) = 2*tmpmat(1,2);
    A(i,3) = 2*tmpmat(1,3);
    A(i,4) = tmpmat(2,2);
    A(i,5) = 2*tmpmat(2,3);
    A(i,6) = tmpmat(3,3);
    A(i,7) = 1;
  }
  return A;
}
inline SymmetricMatrix vec2tens(ColumnVector& Vec){		
  SymmetricMatrix tens(3);
  tens(1,1)=Vec(1);
  tens(2,1)=Vec(2);
  tens(3,1)=Vec(3);
  tens(2,2)=Vec(4);
  tens(3,2)=Vec(5);
  tens(3,3)=Vec(6);
  return tens;
}
float mod(float a, float b){
  while(a>b){a=a-b;}
  while(a<0){a=a+b;} 
  return a;
}

class Alexander_Model:public ForwardModel{
public:
  Alexander_Model(const Matrix& pbvecs,const Matrix& pbvals,int pdebuglevel)
     : ForwardModel(pdebuglevel), bvecs(pbvecs) , bvals(pbvals), alpha(pbvals.Ncols()), beta(pbvals.Ncols()) 
 {
    Amat=form_Amat(bvecs,bvals);
    cart2sph(bvecs,alpha,beta);

   // calculate all the stuff you need later (Q,Gmag,...)
       
	//Gmag.ReSize(bvals);
	DELTA.ReSize(bvals);
	effDiffTime.ReSize(bvals);
	G.ReSize(bvecs);
	qvec.ReSize(bvecs);
		
    for (int i = 1; i <= bvecs.Ncols(); i++){
      //Gmag(1,i)=sqrt(bvals(1,i)/(gamma*gamma*delta*delta*effDiffTime));	
      DELTA(1,i)=bvals(1,i)/(gamma*gamma*Gmag*Gmag*delta*delta) + delta/3;
	  effDiffTime(1,i)= DELTA(1,i)-delta/3;
	  G.Column(i)=Gmag*bvecs.Column(i);	
      qvec.Column(i)=gamma*delta*G.Column(i);
    }		
}	
	  
  ~Alexander_Model(){}
  
  virtual void setparams();
  ReturnMatrix nonlinearfunc(const ColumnVector& paramvalues) const; 
  void initialise(const ColumnVector& S);

private:
  ReturnMatrix sumFactor (const float& R,const float& DX, const Matrix& bessel) const;
  ReturnMatrix vec2difftens(const ColumnVector& v,const float& DL,const float& DT) const;
       
protected:
  
  const Matrix& bvecs;	//bvecs matrix
  const Matrix& bvals;	//bvals matrix
  ColumnVector alpha;
  ColumnVector beta;
  Matrix Amat;	
  
//  const Alexander_Options& opts;
  
  Matrix DELTA;
  Matrix effDiffTime;
  Matrix G;
  Matrix qvec;

};  

ReturnMatrix Alexander_Model::sumFactor (const float& R,const float& DX, const Matrix& bessel) const
{
  Matrix sum_factor;
  float R2=R*R;
  float alpha2;
  float numer,denom;
  sum_factor.ReSize(bvals);
  for (int j=1; j<=bvals.Ncols();j++){
	sum_factor(1,j)=0;
	for (int m=1; m<=bessel.Ncols();m++){
		alpha2 = bessel(1,m)/R2;
		numer= 2*(DX*alpha2*delta + exp(-delta*DX*alpha2) + exp(-DELTA(1,j)*DX*alpha2)) 
		- exp(-(DELTA(1,j)-delta)*DX*alpha2)  -  exp(-(DELTA(1,j)+delta)*DX*alpha2) - 2;
		denom= DX*DX*alpha2*alpha2*alpha2*(bessel(1,m)-1);
		sum_factor(1,j) += (numer/denom);
	}
  }
  sum_factor.Release();
  return sum_factor;
}
ReturnMatrix Alexander_Model::vec2difftens(const ColumnVector& v,const float& DL,const float& DT) const
{
  SymmetricMatrix tens(3);
  float deltaD = DL-DT;
  
  tens.Row(1) << deltaD*v(1)*v(1)+DT; 
  tens.Row(2) << deltaD*v(1)*v(2) << deltaD*v(2)*v(2)+DT;
  tens.Row(3) << deltaD*v(1)*v(3) << deltaD*v(2)*v(3) << deltaD*v(3)*v(3)+DT;
  
  tens.Release();
  return tens;
}

void Alexander_Model::setparams()
{
  clear_params();
  
  UnifPrior S0tmp(0,1000);
  add_param("S0",500,100,S0tmp,true,true);
  
  UnifPrior NUtmp(0,1);
  add_param("NU",0.5,0.05,NUtmp,true,true);
  
  UnifPrior Rtmp(0,0.1); 
  add_param("R",0.005,0.001,Rtmp,true,true);
  // (const string& pname, float pinit_value, float pinit_std, UnifPrior& pprior, bool pallowtovary = true, bool psave = true)
  
  GammaPrior DLtmp(4,1.0/0.0003);
  add_param("DL",0.0015,0.0001,DLtmp,true,true);
  
  GammaPrior DTtmp(4,1.0/0.0003);
  add_param("DT",0.0003,0.00001,DTtmp,true,true);
  
  SinPrior thtmp(1,-1000*M_PI,1000*M_PI);
  add_param("th",0.2,0.02,thtmp,true,true); //Will unwrap thet param before saving
  // (const string& pname, float pinit_value, float pinit_std, SinPrior& pprior, bool pallowtovary = true, bool psave = true)
  
  UnifPrior phtmp(-1000*M_PI,1000*M_PI);
  add_param("ph",0.2, 0.02,phtmp,true,true); //Will unwrap th param before saving
  // (const string& pname, float pinit_value, float pinit_std, UnifPrior& pprior, bool pallowtovary = true, bool psave = true)

//ADD THE NOISE PARAMETER TO BE ESTIMATED HERE!
  GammaPrior RNtmp(1,1);
  add_param("RN",1,0.01,RNtmp,true,true);

}


ReturnMatrix Alexander_Model::nonlinearfunc(const ColumnVector& paramvalues) const
{
  // param(1) = S0
  // param(2) = NU
  // param(3) = R     (mm)
  // param(4) = DL    (mm2/s)
  // param(5) = DT    (mm2/s)
  // param(6) = th    (rad)
  // param(7) = ph    (rad)
  // param(8) = RN    (sigma value)
  
  float S0 = paramvalues(1);
  float NU = paramvalues(2);
  float R  = paramvalues(3);
  float DL = paramvalues(4);
  float DT = paramvalues(5);
  float th = paramvalues(6);
  float ph = paramvalues(7);
 // float RN = paramvalues(8);
  
  float one_NU = 1-NU;
  Matrix Dh;
  float Gpar, Gper;
  
  ColumnVector vec(3);	
		  vec	<< sin(th)*cos(ph)
				<< sin(th)*sin(ph)
				<< cos(th);
				
  	  float modvec=sqrt(vec(1)*vec(1)+vec(2)*vec(2)+vec(3)*vec(3));
      if(modvec!=0){	//MAKE UNITARY VECTORS
		vec(1)=vec(1)/modvec;
		vec(2)=vec(2)/modvec;
		vec(3)=vec(3)/modvec;
	  }
  
  Dh = vec2difftens(vec,DL,DT);
  
  float dd2=delta*delta;
  float gg2=gamma*gamma;
  
  Matrix sum_factor = sumFactor(R, DT, Besselroots2);
  ColumnVector ret(bvals.Ncols());

  float Sh,Sr,cosang;
  for (int i = 1; i <= ret.Nrows(); i++){
//float R2 = R*R;
    Sh = exp( - effDiffTime(1,i) * (qvec.Column(i).t() * Dh * qvec.Column(i)).AsScalar() );
    cosang=abs(dot(bvecs.Column(i),vec));
	if (bvals(1,i)==0){
		Sr = 1;
	}else{
		Gpar = Gmag*cosang;
		Gper = Gmag*sqrt(1-cosang*cosang);
	Sr = exp( -(gg2*(effDiffTime(1,i)*dd2*Gpar*Gpar*DL + 2*Gper*Gper*sum_factor(1,i))) );
//	Sr = exp( -(gg2*(effDiffTime(1,i)*dd2*Gpar*Gpar*DL + (R2*R2*Gper*Gper/DT)*(7/96)*(2*(DELTA(1,i)+delta)-(99/112)*R2/DT)) ));
    }
    ret(i) = S0 * (one_NU * Sh + NU * Sr );
	
  }
    				
  ret.Release();
  return ret; 
  
}


void Alexander_Model::initialise(const ColumnVector& S){
  
  ColumnVector logS(S.Nrows()),tmp(S.Nrows()),Dvec(7),dir(3);
  
  SymmetricMatrix tens;   //Basser's Diffusion Tensor;
  DiagonalMatrix Dd;   //eigenvalues
  Matrix Vd;   //eigenvectors
  float mDd,fsquared;
  float S0,th,ph,f,D;
  float sigma = 0;
  float m=0,s2=0,n=0;

  for ( int i = 1; i <= S.Nrows(); i++)
    {
	  if (bvals(1,i)==0){
		n++;
		m += S(i);
		s2 += S(i)*S(i);
	  }
      if(S(i)>0){
	logS(i)=log(S(i));
      }
      else{
	logS(i)=0;
      }
    }
  Dvec = -pinv(Amat)*logS;
  if(  Dvec(7) >  -maxlogfloat ){
    S0=exp(-Dvec(7));
  }
  else{
    S0=S.MaximumAbsoluteValue();
  }
  
  for ( int i = 1; i <= S.Nrows(); i++){
    if(S0<S.Sum()/S.Nrows()){ S0=S.MaximumAbsoluteValue();  }
    logS(i)=(S(i)/S0)>0.01 ? log(S(i)):log(0.01*S0);
  }
  
  Dvec = -pinv(Amat)*logS;
  S0=exp(-Dvec(7));
  if(S0<S.Sum()/S.Nrows()){ S0=S.Sum()/S.Nrows();  }
  
  if(n>1){
	sigma = (s2-m*m/n) / n;
  }else{
	sigma = 0.01;
  }
  
  tens = vec2tens(Dvec);
  EigenValues(tens,Dd,Vd);
  mDd = Dd.Sum()/Dd.Nrows();
  int maxind = Dd(1) > Dd(2) ? 1:2;   //finding maximum eigenvalue
  maxind = Dd(maxind) > Dd(3) ? maxind:3;
  
  dir << Vd(1,maxind) << Vd(2,maxind) << Vd(3,maxind);
  cart2sph(dir,th,ph); 
  th= mod(th,M_PI);
  ph= mod(ph,2*M_PI);
  
  D = Dd(maxind);
  
  float numer2=(1.5*(Dd(1)-mDd)*(Dd(1)-mDd)+(Dd(2)-mDd)*(Dd(2)-mDd)+(Dd(3)-mDd)*(Dd(3)-mDd));
  float denom2=(Dd(1)*Dd(1)+Dd(2)*Dd(2)+Dd(3)*Dd(3));
  if(denom2>0) fsquared=numer2/denom2;
  else fsquared=0;
  if(fsquared>0){f=sqrt(fsquared);}
  else{f=0;}
  if(f>=0.95) f=0.95;
  if(f<=0.001) f=0.001;
  
  float NU = f; // 1-f???
  float R  = 0.9e-3;

  float DL = D;		//Dd(maxind); 
  float DT = (3*mDd-DL)/2;
  float RN = sigma*sigma;
  
  cout << "After Initialisation" << endl;
  OUT(S0);
  OUT(NU);
  OUT(R);
  OUT(DL);
  OUT(DT);
  OUT(th);
  OUT(ph);
  OUT(RN);
	
	
  getparam(0).setinitvalue(S0);
  getparam(1).setinitvalue(NU);
  getparam(2).setinitvalue(R);
  getparam(3).setinitvalue(DL);
  getparam(4).setinitvalue(DT);
  getparam(5).setinitvalue(th);
  getparam(6).setinitvalue(ph);
  getparam(7).setinitvalue(RN);

}


int main(int argc, char *argv[])
{
  try{
    
    // Setup logging:
    Log& logger = LogSingleton::getInstance();
    
    // parse command line - will output arguments to logfile
    Alexander_Options& opts = Alexander_Options::getInstance();	
    opts.parse_command_line(argc, argv, logger);
    //srand(Alexander_Options::getInstance().seed.value());
    srand(opts.seed.value());
    
    if(opts.debuglevel.value()==1)
      Tracer_Plus::setrunningstackon();
    
    if(opts.timingon.value())
      Tracer_Plus::settimingon();


    // read data
    volume4D<float> input;
    read_volume4D(input,opts.datafile.value());  
    int ntpts=input.tsize();
    volume4D<float> mask;
    read_volume4D(mask,opts.maskfile.value());
    mask.threshold(1e-16,mask.max()+1,inclusive);
    Matrix data=input.matrix(mask[0]);

    cout << "ntpts=" << ntpts << endl;
    cout << "nvoxels=" << mask.sum() << endl;
    
    Matrix bvecs = read_ascii_matrix(opts.bvecsfile.value());	//BVECS
    if(bvecs.Nrows()>3) bvecs=bvecs.t();
    for(int i=1;i<=bvecs.Ncols();i++){
      float tmpsum=sqrt(bvecs(1,i)*bvecs(1,i)+bvecs(2,i)*bvecs(2,i)+bvecs(3,i)*bvecs(3,i));
      if(tmpsum!=0){	//MAKE UNITARY VECTORS
	bvecs(1,i)=bvecs(1,i)/tmpsum;
	bvecs(2,i)=bvecs(2,i)/tmpsum;
	bvecs(3,i)=bvecs(3,i)/tmpsum;
      }  
    }
    
    Matrix bvals = read_ascii_matrix(opts.bvalsfile.value());	//BVALS
   
    besselroots = read_ascii_matrix(opts.besselfile.value()); //BESSEL DERIVATIVE ROOTS

	Besselroots2.ReSize(1,60);  
    for(int i=2;i<=besselroots.Ncols();i++)
	   Besselroots2(1,i-1) = besselroots(1,i)*besselroots(1,i);
    
    Alexander_Model model(bvecs,bvals,opts.debuglevel.value());
    
    LSMCMCManager lsmcmc(opts,model,data,mask);
    LSLaplaceManager lslaplace(opts,model,data,mask);
    
    if(opts.inference.value()=="mcmc"){
      lsmcmc.setup();
      lsmcmc.run();
      element_mod_n(lsmcmc.getsamples(5),M_PI);
      element_mod_n(lsmcmc.getsamples(6),2*M_PI);
      lsmcmc.save();
   }
    else{
      lslaplace.setup();
      lslaplace.run();
      lslaplace.save();
    }
    
	
//	if(opts.noisemodel.value()=="g")
//	{
//	  cout << "Gaussian noise model chosen" << endl;
	  Matrix data_out;
	  
	  data_out.ReSize(bvals.Ncols(),1);
	  ColumnVector param1(7);

	  param1 << 500 << 0.2 << 0.002 << 0.0017 << 0.0002 << M_PI/4 << 0 << 0.3973;
	  data_out.Column(1) = model.nonlinearfunc(param1);

	  write_ascii_matrix(data_out,"/usr/people/rsance/scratch/outcomes/testdatafromcc");
//	}
//	else if(opts.noisemodel.value()=="r")
//	{
//		cout << "Rician noise model chosen, not implemented" << endl;
//	}
	
			
    if(opts.timingon.value())
      Tracer_Plus::dump_times(logger.getDir());
    
    cout << endl << "Log directory was: " << logger.getDir() << endl;
    
  }
  catch(Exception& e) 
    {
      cerr << endl << e.what() << endl;
    }
  catch(X_OptionError& e) 
    {
      cerr << endl << e.what() << endl;
    }
  
  return 0;
  
}  

  
