include $(FSLCONFDIR)/default.mk

PROJNAME = fmrib07

USRINCFLAGS = -I${INC_NEWMAT} -I${INC_NEWRAN} -I${INC_CPROB} -I${INC_PROB} -I${INC_ZLIB}
USRLDFLAGS = -L${LIB_NEWMAT} -L${LIB_NEWRAN} -L${LIB_CPROB} -L${LIB_PROB} -L${LIB_ZLIB}

DLIBS = -lmeshclass -lnewimage -lutils -lmiscmaths -lnewmat -lnewran -lNewNifti -lznz -lcprob -lprob -lm -lz


AM=alexander_model

AMOBJS=alexander_model.o alexander_options.o mybintoptions.o mylsmcmcmanager.o mylslaplacemanager.o

all: ${AM}

${AM}:		   ${AMOBJS}
		   ${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ ${AMOBJS} ${DLIBS}

